介绍

这里就不对 java？、JDK？、JRE？... 等内容进行认知介绍，上述内容百度已经一搜一大堆，如果你还不够了解请自行百度或点击下方相应的链接进行学习。

Java基础教程（http://www.runoob.com/java/java-tutorial.html）
Java？（https://baike.baidu.com/item/Java/85979?fr=aladdin）
JDK？（https://baike.baidu.com/item/jdk/1011?fr=aladdin）
JRE？（https://baike.baidu.com/item/JRE/2902404?fr=aladdin）
JVM？（https://baike.baidu.com/item/JVM/2902369?fr=aladdin）
JDK、JRE、JVM有什么区别？有什么关系？（https://jingyan.baidu.com/article/425e69e6077283be15fc16ed.html）

常用JDK版本（本站内容会围绕这个版本）
Oracle Java（https://www.oracle.com/technetwork/java/index.html）

其他JDK版本
Opend JDK （http://openjdk.java.net）
IBM JDK （https://www.ibm.com/developerworks/java/jdk/）



下载安装和配置

下载地址

Oracle下载地址（推荐）
https://www.oracle.com/technetwork/java/javase/downloads/index.html

找到相应的版本 向下可以找到历史版本，点击JDK中的DOWNLOAD按钮进入相应下载页面

记得勾选Accept License Agreement（接收许可协议），好多人说无法下载就是没有勾选协议

如果Oracle网址打不开或者下载缓慢，下面有我上传的百度云资源，不过还是推荐使用Oracle网址下载

百度云下载地址
https://pan.baidu.com/s/1AA2mwlQbKGeTQ0_PxORfMg  提取码：lume

安装教程（https://jingyan.baidu.com/article/6dad5075d1dc40a123e36ea3.html）

教程中的环境变量

JAVA_HOME C:\Program Files\Java\jdk1.8.0_181
Path  %JAVA_HOME%\bin;%JAVA_HOME%\jre\bin;
CLASSPATH  .;%JAVA_HOME%\lib;%JAVA_HOME%\lib\tools.jar

Path和CLASSPATH 直接复制即可，JAVA_HOME要复制你安装JDK（重要是JDK不是Jre）的安装目录

JDK8 版本的JDK可以不手设置环境变量，如果你这是在Eclipse,IDEA等工具中使用Java也可以不设置环境变量